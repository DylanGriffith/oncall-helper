require 'sinatra'
require 'rest-client'
require 'yaml'
require 'json'

get '/' do
  "Ok"
end

get '/team.json' do
  response = RestClient.get('https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml')

  data = YAML.load(response.body)

  content_type :json
  data.to_json
end

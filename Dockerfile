FROM ruby:2.6.4
ADD ./ /app/
WORKDIR /app
ENV RACK_ENV production
ENV PORT 5000
EXPOSE 5000

RUN bundle install
CMD ["bundle","exec", "ruby", "app.rb", "-p" , "5000"]
